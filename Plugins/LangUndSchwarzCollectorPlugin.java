import currency.Currency;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.slf4j.Logger;
import price.Price;
import product.Product;
import product.ProductPrice;
import service.collector.api.CollectorPlugin;
import service.collector.exception.ProductNotFoundException;
import service.collector.exception.URLNotSupportedException;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.ZonedDateTime;
import java.util.Locale;
import java.util.Optional;
import java.util.regex.Pattern;

public class LangUndSchwarzCollectorPlugin implements CollectorPlugin {
    private Logger LOGGER;

    @Override
    public void init(WebDriver driver, Product product, Currency currency, Logger logger) throws ProductNotFoundException {
        LOGGER = logger;
        WebElement acceptButton = driver.findElement(By.cssSelector(".accept"));
        acceptButton.click();

        WebElement searchBar = driver.findElement(By.id("sitesearch"));
        searchBar.click();
        new Actions(driver)
                .moveToElement(searchBar)
                .sendKeys(product.getName())
                .perform();

        WebElement searchResultsBox = driver.findElement(By.cssSelector(".tt-dataset-instrumentsearch"));
        Optional<WebElement> searchResult = searchResultsBox.findElements(By.cssSelector(".tt-suggestion")).stream()
                .filter(resultElement -> {
                    WebElement displayNameElement = resultElement.findElement(By.cssSelector(".displayName"));
                    String displayName = (String) ((JavascriptExecutor)driver).executeScript("return arguments[0].textContent;", displayNameElement);
                    return displayName.equalsIgnoreCase(product.getName());
                }).findFirst();
        if (!searchResult.isPresent()){
            throw new ProductNotFoundException(String.format("Did not find product '%s'", product.getName()));
        }
        searchResult.get().click();
    }

    @Override
    public Optional<ProductPrice> extract(WebDriver driver, Product product, Currency currency, Logger logger) throws URLNotSupportedException {
        WebElement pageContent = driver.findElement(By.id("page_content"));
        WebElement priceContainer = pageContent.findElement(By.cssSelector(".row .col-md-8 .mono"));
        WebElement priceField = priceContainer.findElement(By.cssSelector("span[field=\"mid\"]"));

        String priceString = (String) ((JavascriptExecutor) driver).executeScript("return arguments[0].textContent", priceField);
        Optional<Locale> locale;
        try {
            locale = getLocale(driver);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
        if (!locale.isPresent()){
            throw new URLNotSupportedException(String.format("Support language of the current URL: %s", driver.getCurrentUrl()));
        }
        DecimalFormat format = (DecimalFormat) NumberFormat.getInstance(locale.get());
        format.setParseBigDecimal(true);
        BigDecimal priceValue;
        try {
            priceValue = (BigDecimal) format.parseObject(priceString);
        } catch (ParseException e) {
            LOGGER.error(String.format("Could not parse price string to BigDecimal using locale %s: '%s'", locale.get().getDisplayLanguage(Locale.ENGLISH), priceString));
            return Optional.empty();
        }

        return Optional.of(new ProductPrice(product, new Price(priceValue, currency), ZonedDateTime.now()));
    }

    private Optional<Locale> getLocale(WebDriver driver) throws MalformedURLException {
        String fileSpecifier = new URL(driver.getCurrentUrl()).getFile();

        String[] parts = fileSpecifier.split("/");
        if (parts.length < 2){
            return Optional.empty();
        }
        String language = parts[1];

        if (language.equalsIgnoreCase("de")){
            return Optional.of(Locale.GERMAN);
        }
        if (language.equalsIgnoreCase("en")){
            return Optional.of(Locale.ENGLISH);
        }
        return Optional.empty();
    }

    @Override
    public Pattern getCompatibleURLsPattern() {
        return Pattern.compile(".*");
    }

    @Override
    public String getName() {
        return "Lang & Schwarz Collector Plugin";
    }

    @Override
    public Optional<String> getDescription() {
        return Optional.of("This plugins queries prices from the website of LANG & SCHWARZ Tradecenter AG & Co. KG");
    }
}
