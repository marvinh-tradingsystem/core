package api;

import api.address.Address;

public class Market {
    /**
     * The market identification code. It is a unique identifier for a trading platform.
     */
    public static class MIC{
        private final String mic;

        public MIC(String mic) {
            assert mic != null;
            this.mic = mic;
        }

        public String getMic() {
            return mic;
        }
    }

    private final String name;
    private final Address location;
    private final MIC mic;

    public Market(MIC mic, Address location, String name) {
        assert name != null;
        assert mic != null;
        assert location != null;

        this.name = name;
        this.mic = mic;
        this.location = location;
    }

    public String getName(){
        return name;
    }

    public MIC getMic() {
        return mic;
    }

    public Address getLocation() {
        return location;
    }
}
