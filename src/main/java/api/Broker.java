package api;

import api.order.Order;

public interface Broker {
    String getName();
    void placeOrder(Order order);
}
