package api.address;

public class Country {
    private final String englishName;
    private final String countryCode;

    public Country(String englishName, String countryCode) {
        this.englishName = englishName;
        this.countryCode = countryCode;
    }

    public String getEnglishName() {
        return englishName;
    }

    public String getCountryCode() {
        return countryCode;
    }
}
