package api.order.validity;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Optional;

/**
 * GTD-orders are valid until the given date (and time, if specified)
 */
public class GTD implements Validity {
    private final LocalDate localDate; //date until an order is valid
    private LocalTime localTime; //time until an order is valid

    public GTD(LocalDate localDate) {
        this.localDate = localDate;
    }

    public GTD(LocalDate localDate, LocalTime localTime) {
        this.localDate = localDate;
        this.localTime = localTime;
    }

    public LocalDate getLocalDate() {
        return localDate;
    }

    public Optional<LocalTime> getLocalTime(){
        return Optional.ofNullable(localTime);
    }
}
