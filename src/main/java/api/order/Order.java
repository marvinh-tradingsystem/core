package api.order;

import api.order.additionals.Additional;
import api.order.validity.Validity;
import product.Product;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Set;

public class Order {
    public enum TradeType{
        BUY,
        SELL
        ;
    }

    private final Product product; //the product to trade
    private final BigDecimal amount; //the amount of products to trade
    private final TradeType tradeType; //the type of the trade
    private final Validity validity; //the validity of the order
    private final Set<Additional> additionals; //set of order additionals (Limit, Stopp-Loss, OCO, etc.)

    public Order(Product product, BigDecimal amount, TradeType tradeType, Validity validity) {
        assert product != null;
        assert amount != null;
        assert tradeType != null;
        assert validity != null;

        this.product = product;
        this.amount = amount;
        this.tradeType = tradeType;
        this.validity = validity;
        this.additionals = Collections.emptySet();
    }

    public Order(Product product, BigDecimal amount, TradeType tradeType, Validity validity, Set<Additional> additionals) {
        assert product != null;
        assert amount != null;
        assert tradeType != null;
        assert validity != null;
        assert additionals != null;

        this.product = product;
        this.amount = amount;
        this.tradeType = tradeType;
        this.validity = validity;
        this.additionals = additionals;
    }

    public Product getProduct(){
        return product;
    }
    public BigDecimal getAmount(){
        return amount;
    }
    public TradeType getTradeType(){
        return tradeType;
    }

    public Validity getValidity() {
        return validity;
    }

    public Set<Additional> getAdditionals() {
        return additionals;
    }
}
