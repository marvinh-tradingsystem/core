package api.order.additionals;

import api.order.Order;

/**In addition to the order (refered as "main order"), also place the second order, which is specified here, on the market.
 * When the main order is executed, the second order is striked from the order book.
 * When the second order is executed, the main order is striked from the order book.
 */
public class OCO implements Additional{
    private final Order order;

    public OCO(Order order) {
        assert order != null;
        this.order = order;
    }

    public Order getOrder() {
        return order;
    }
}
