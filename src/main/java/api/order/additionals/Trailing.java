package api.order.additionals;

import price.Gap;

/**
 * Set the stop to a given gap from the current price.
 * On a BUY order, the stop is lowered each time the price falls and its gap to the stop is higher than the specified gap.
 * When the price raises up to the stop, the order is changed into a BUY Market Order.
 * On a SELL order, the stop is raised each time the price raises and its gap to the stop is higher than the specified gap.
 * When the price falls down to the stop, the order is changed into a SELL Market Order.
 * Note: The gap refers to the price per each piece
 * and can be given absolute (use Price class) or relative (use GapInPercent class)
 */
public class Trailing implements Additional{
    private final Gap gap;

    public Trailing(Gap gap) {
        assert gap != null;
        this.gap = gap;
    }

    public Gap getGap() {
        return gap;
    }
}
