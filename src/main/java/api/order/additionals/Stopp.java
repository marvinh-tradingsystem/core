package api.order.additionals;

import price.Price;

/**
 * Execute order only when it matches the stop price
 * On a BUY order, the order is executed when the price is higher or equal the stop price (Stop-Buy)
 * On a SELL order, the order is executed when the price is lower or equal the stop price (Stop-Loss)
 * Note: The Limit is the price per each piece
 */
public class Stopp implements Additional{
    private final Price stop;

    public Stopp(Price stop) {
        assert stop != null;
        this.stop = stop;
    }

    public Price getStop() {
        return stop;
    }
}
