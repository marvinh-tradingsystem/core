package api.order.additionals;

import price.Price;

/**
 * Execute order only when it matches the specified limit.
 * On a BUY order, the order is executed only when the price is lower or equal the limit.
 * On a SELL order, the order is executed only when the price is higher or equal the limit.
 * Note: The Limit is the price per each piece
 */
public class Limit implements Additional {
    private final Price limit;

    public Limit(Price limit) {
        assert limit != null;
        this.limit = limit;
    }

    public Price getLimit() {
        return limit;
    }
}
