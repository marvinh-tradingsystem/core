package api.order.additionals;

import api.order.Order;

/**
 * When the order is executed, also execute the second order specified here
 */
public class IfDone implements Additional{
    private final Order order;

    public IfDone(Order order) {
        assert order != null;
        this.order = order;
    }

    public Order getOrder() {
        return order;
    }
}
