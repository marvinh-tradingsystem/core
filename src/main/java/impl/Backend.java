package impl;

import database.schema.DatabaseSchema;
import databaseconnector.impl.H2PersistentDatabaseConnection;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletHandler;
import service.collector.CollectorService;
import service.collector.api.Collector;
import service.collector.api.CollectorPlugin;
import service.database.SQLDatabaseService;
import socket.Port;

import java.io.File;
import java.net.URL;
import java.util.Collection;

public class Backend {
    private static Port port;
    private static Server server;

    private static boolean databaseService = false;
    private static boolean collectorService = false;

    public static void activateDatabaseService(File databaseFile){
        Backend.databaseService = true;
        SQLDatabaseService.setDatabaseConnection(new H2PersistentDatabaseConnection(databaseFile.getAbsoluteFile()));
        SQLDatabaseService.setSchema(new DatabaseSchema());
        SQLDatabaseService.start();
    }

    public static void activateCollectorService(URL databaseEndpoint, Collection<Collector> collectors, Collection<CollectorPlugin> collectorPlugins) {
        Backend.collectorService = true;
        CollectorService.reset();
        CollectorService.setDatabaseEndpoint(databaseEndpoint);
        collectors.forEach(collector -> {
            try {
                CollectorService.addCollector(collector);
            } catch (Exception exception) {
                throw new RuntimeException(exception);
            }
        });
        collectorPlugins.forEach(CollectorService::addCollectorPlugin);
        CollectorService.start();
    }

    public static void run(Port port) throws Exception {
        assert port != null;
        Backend.port = port;
        Backend.server = new Server();
        ServerConnector connector = new ServerConnector(server);
        connector.setPort(port.getNumber());
        server.setConnectors(new Connector[] {connector});
        Backend.server.setHandler(createServletHandler());
        Backend.server.start();

        if (databaseService){
            SQLDatabaseService.start();
        }
        if (collectorService){
            CollectorService.start();
        }
    }

    private static ServletHandler createServletHandler() {
        ServletHandler servletHandler = new ServletHandler();

        SQLDatabaseService.registerCommands(servletHandler);
        CollectorService.registerCommands(servletHandler);
        return servletHandler;
    }

    public static Port getPort(){
        return Backend.port;
    }
}
