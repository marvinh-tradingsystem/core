package impl.tasks;

import impl.Backend;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import socket.Port;

public class StartBackend extends AbstractTask{
    private static final Logger LOGGER = LoggerFactory.getLogger(StartBackend.class);

    private final Port port;

    public StartBackend(Port port) {
        assert port != null;
        this.port = port;
    }

    @Override
    public void run() {
        try {
            Backend.run(port);
            LOGGER.info(String.format("Backend started on port %d", port.getNumber()));
            finished.set(true);
        } catch (Exception exception) {
            LOGGER.error(exception.getMessage());
            aborted.set(true);
        }
    }

    public Port getPort() {
        return port;
    }
}
