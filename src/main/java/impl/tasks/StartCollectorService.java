package impl.tasks;

import com.gargoylesoftware.htmlunit.*;
import com.google.gson.Gson;
import currency.Currency;
import database.schema.CollectorPluginTable;
import database.schema.CurrencyTable;
import database.schema.ProductTable;
import impl.Backend;
import impl.config.settings.roles.CollectorRoleConfiguration;
import impl.config.settings.roles.collector.CollectorConfiguration;
import impl.exception.CurrencyNotFoundException;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pluginloader.PluginLoader;
import product.Product;
import service.collector.api.Collector;
import service.collector.api.CollectorPlugin;
import service.collector.impl.BlockedWaitingCollector;
import service.collector.impl.DOMChangeListeningWebDriver;
import service.collector.impl.demomode.DemoPage;
import service.database.command.DatabaseInitiated;
import service.database.exception.DatabaseNotInitiatedException;
import service.database.response.QueryRows;

import javax.annotation.Nullable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;

public class StartCollectorService extends AbstractTask{
    private static class InitializationStatusCheckException extends Exception{
        public InitializationStatusCheckException(String message) {
            super(message);
        }
    }

    private static class CollectorStatus{
        private final Set<CollectorConfiguration> collectorsToStart = new HashSet<>();
        private final Set<Collector> collectorsReady = new HashSet<>();

        public CollectorStatus(Set<CollectorConfiguration> collectorsToStart) {
            assert collectorsToStart != null;
            this.collectorsToStart.addAll(collectorsToStart);
        }
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(StartCollectorService.class);

    private static final long retryInterval = 5;
    private static final TimeUnit timeUnit = TimeUnit.SECONDS;

    private final StartDatabaseService startDatabaseService;
    private final StartBackend startBackend;
    private final URL databaseEndpoint;

    private final ScheduledThreadPoolExecutor threadPoolExecutor;

    private final CollectorStatus collectorStatus;

    public StartCollectorService(@Nullable StartDatabaseService startDatabaseService, StartBackend startBackend, CollectorRoleConfiguration configuration, ScheduledThreadPoolExecutor threadPoolExecutor) throws MalformedURLException {
        assert startBackend != null;
        assert configuration != null;
        assert configuration.getDatabaseEndpoint() != null;
        assert configuration.getCollectors() != null;
        assert threadPoolExecutor != null;

        this.startDatabaseService = startDatabaseService;
        this.startBackend = startBackend;
        this.databaseEndpoint = configuration.getDatabaseEndpoint();
        this.collectorStatus = new CollectorStatus(configuration.getCollectors());

        this.threadPoolExecutor = threadPoolExecutor;
    }

    private StartCollectorService(@Nullable StartDatabaseService startDatabaseService, StartBackend startBackend, URL databaseEndpoint, CollectorStatus collectorStatus, ScheduledThreadPoolExecutor threadPoolExecutor) {
        assert startBackend != null;
        assert databaseEndpoint != null;
        assert collectorStatus != null;
        assert threadPoolExecutor != null;

        this.databaseEndpoint = databaseEndpoint;
        this.collectorStatus = collectorStatus;
        this.startDatabaseService = startDatabaseService;
        this.startBackend = startBackend;

        this.threadPoolExecutor = threadPoolExecutor;
    }

    @Override
    public void run() {
        if (startDatabaseService != null){
            if (startDatabaseService.isAborted()){
                LOGGER.error("Database service crashed. Can not start collector service.");
                return;
            }
            if (!startDatabaseService.isFinished()){
                LOGGER.info("Waiting for database service to start.");
                threadPoolExecutor.schedule(
                        new StartCollectorService(startDatabaseService, startBackend, databaseEndpoint, collectorStatus, threadPoolExecutor),
                        retryInterval, timeUnit
                );
                return;
            }
        }

        try{
            if (!isDatabaseInitiated()){
                LOGGER.warn("Database is not initiated, so the collectors can not start. Please initiate the database.");
                threadPoolExecutor.schedule(
                        new StartCollectorService(startDatabaseService, startBackend, databaseEndpoint, collectorStatus, threadPoolExecutor),
                        retryInterval, timeUnit
                );
                return;
            }
        } catch (InitializationStatusCheckException e) {
            LOGGER.error(String.format("Failed checking if database is initiated: %s", e.getMessage()));
            e.printStackTrace();
            return;
        } catch (IOException e){
            throw new RuntimeException(e);
        }

        try {
            URL demoPageURL = new URL(String.format("http://localhost:%s%s", startBackend.getPort(), DemoPage.getPath()));

            Map<Collector, CollectorConfiguration> collectorsToInit = new HashMap<>();
            collectorStatus.collectorsToStart.forEach(collectorConfiguration -> {
                assert collectorConfiguration.getChangeListening().getType() == CollectorConfiguration.ChangeListening.Type.BLOCKEDWAITING;
                DOMChangeListeningWebDriver.DOMPollInterval pollInterval = new DOMChangeListeningWebDriver.DOMPollInterval(
                        collectorConfiguration.getChangeListening().getDomChangeCheck().getRate(),
                        collectorConfiguration.getChangeListening().getDomChangeCheck().getUnit()
                );

                collectorConfiguration.getProductsAndCurrencies().forEach((productDescription, currencyDescription) -> {
                    try {
                        Optional<Product> product = loadProductFromDatabase(productDescription);
                        if (!product.isPresent()){
                            LOGGER.warn(String.format("Product '%s' has not been found in the database. Please add it.", productDescription.getName()));
                            threadPoolExecutor.schedule(
                                    new StartCollectorService(startDatabaseService, startBackend, databaseEndpoint, collectorStatus, threadPoolExecutor),
                                    retryInterval, timeUnit
                            );
                            return;
                        }
                        Currency currency = loadCurrencyFromDatabase(currencyDescription);
                        Collector collector = new BlockedWaitingCollector(collectorConfiguration.getQueryURL(), product.get(), currency, databaseEndpoint, pollInterval, demoPageURL);
                        collectorsToInit.put(collector, collectorConfiguration);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    } catch (DatabaseNotInitiatedException e) {
                        LOGGER.warn("Database is not initiated, so the collectors can not start. Please initiate the database.");
                        threadPoolExecutor.schedule(
                                new StartCollectorService(startDatabaseService, startBackend, databaseEndpoint, collectorStatus, threadPoolExecutor),
                                retryInterval, timeUnit
                        );
                    } catch (CurrencyNotFoundException e) {
                        LOGGER.warn(String.format("Currency with ID '%s' was not found, so the collector can not start. Please add the currency to the database.", currencyDescription.getId()));
                        threadPoolExecutor.schedule(
                            new StartCollectorService(startDatabaseService, startBackend, databaseEndpoint, collectorStatus, threadPoolExecutor),
                            retryInterval, timeUnit
                        );
                    }
                });
            });

            Set<CollectorPlugin> collectorPlugins = loadCollectorPlugins();
            LOGGER.info("Collector plugins have been loaded from the database.");

            collectorsToInit.forEach((collector, collectorConfiguration) -> {
                try {
                    Optional<CollectorPlugin> collectorPlugin = loadAssignedCollectorPlugin(collectorConfiguration.getPluginName(), collectorPlugins);
                    if (!collectorConfiguration.getPluginName().isEmpty()){
                        if (!collectorPlugin.isPresent()){
                            LOGGER.warn(String.format("Collector plugin '%s' was not found. Collector can not start", collectorConfiguration.getPluginName()));
                            return;
                        }
                        collector.initNormal(collectorPlugin.get());
                    }
                    if (collectorConfiguration.getDemoMode()){
                        collector.initDemo(true);
                    }
                    if (collectorConfiguration.getPluginName().isEmpty() || collectorPlugin.isPresent()){
                        collectorStatus.collectorsToStart.remove(collectorConfiguration);
                        collectorStatus.collectorsReady.add(collector);
                    }
                } catch (Exception exception) {
                    throw new RuntimeException(exception);
                }
            });

            if (!collectorStatus.collectorsToStart.isEmpty()) {
                threadPoolExecutor.schedule(
                        new StartCollectorService(startDatabaseService, startBackend, databaseEndpoint, collectorStatus, threadPoolExecutor),
                        retryInterval, timeUnit
                );
            }
            Backend.activateCollectorService(this.databaseEndpoint, collectorStatus.collectorsReady, collectorPlugins);
            this.finished.set(true);
            LOGGER.info("Collector role activated.");
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (DatabaseNotInitiatedException e) {
            LOGGER.warn("Database is not initiated, so the collectors can not start. Please initiate the database.");
            threadPoolExecutor.schedule(
                    new StartCollectorService(startDatabaseService, startBackend, databaseEndpoint, collectorStatus, threadPoolExecutor),
                    retryInterval, timeUnit
            );
        }
    }

    private Optional<CollectorPlugin> loadAssignedCollectorPlugin(String pluginName, Set<CollectorPlugin> collectorPlugins) {
        assert collectorPlugins != null;
        return collectorPlugins.stream().filter(collectorPlugin -> collectorPlugin.getName().equals(pluginName)).findAny();
    }

    private QueryRows queryTable(String tableName) throws IOException, DatabaseNotInitiatedException {
        List<String> queryParameters = new ArrayList<>();
        queryParameters.add(String.format("table=%s", URLEncoder.encode(tableName, StandardCharsets.UTF_8)));
        URL requestURL = new URL(databaseEndpoint, String.format("command/database/query?%s", String.join("&", queryParameters.toArray(new CharSequence[0]))));
        WebRequest webRequest = new WebRequest(requestURL, HttpMethod.GET);

        WebClient webClient = new WebClient();
        webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
        Page page = webClient.getPage(webRequest);
        WebResponse response = page.getWebResponse();
        if (response.getStatusCode() == 500){
            throw new DatabaseNotInitiatedException(response.getContentAsString());
        }
        if (response.getStatusCode() >= 300){
            throw new RuntimeException(String.format("Error querying URL:\n%s", response.getContentAsString()));
        }
        InputStream contentAsStream = response.getContentAsStream();
        return new Gson().fromJson(new InputStreamReader(contentAsStream, StandardCharsets.UTF_8), QueryRows.class);
    }

    private Optional<Product> loadProductFromDatabase(CollectorConfiguration.Product productDescription) throws IOException, DatabaseNotInitiatedException {
        return queryTable(new ProductTable().getName()).stream()
                .filter(responseEntry -> {
                    assert responseEntry.getTable().equalsIgnoreCase(new ProductTable().getName());
                    Optional<Map<String, String>> productRow = findProductRow(productDescription, responseEntry);
                    return productRow.isPresent();
                })
                .map(new Function<QueryRows.ResponseEntry, Product>() {
                    @Override
                    public Product apply(QueryRows.ResponseEntry responseEntry) {
                        assert responseEntry.getTable().equalsIgnoreCase(new ProductTable().getName());
                        Optional<Map<String, String>> productRow = findProductRow(productDescription, responseEntry);
                        assert productRow.isPresent();

                        Optional<String> productNameKey = findKey(productRow.get(), ProductTable.NAME.getName());
                        assert productNameKey.isPresent();

                        Product product = () -> productRow.get().get(productNameKey.get());
                        return product;
                    }
                })
                .findAny();
    }

    @NotNull
    private Optional<Map<String, String>> findProductRow(CollectorConfiguration.Product productDescription, QueryRows.ResponseEntry responseEntry) {
        return responseEntry.getEntries().stream().filter(row -> {
            Optional<String> nameKey = findKey(row, ProductTable.NAME.getName());
            if (!nameKey.isPresent()) {
                return false;
            }
            String name = row.get(nameKey.get());
            return name.equals(productDescription.getName());
        }).findAny();
    }

    private Currency loadCurrencyFromDatabase(CollectorConfiguration.Currency currencyDescription) throws IOException, DatabaseNotInitiatedException, CurrencyNotFoundException {
        AtomicReference<Currency> currency = new AtomicReference<>(null);
        queryTable(new CurrencyTable().getName()).forEach(responseEntry -> {
            assert responseEntry.getTable().equalsIgnoreCase(new CollectorPluginTable().getName());
            Optional<Map<String, String>> row = responseEntry.getEntries().stream().filter(row1 -> {
                Optional<String> key = findKey(row1, CurrencyTable.ABBREVIATION.getName());
                return key.isPresent() && row1.get(key.get()).equals(currencyDescription.getId());
            }).findAny();
            if (!row.isPresent()){
                return;
            }
            Optional<String> nameKey = findKey(row.get(), CurrencyTable.NAME.getName());
            Optional<String> symbolKey = findKey(row.get(), CurrencyTable.SYMBOL.getName());
            assert nameKey.isPresent();
            assert symbolKey.isPresent();
            String currencyName = row.get().get(nameKey.get());
            String currencySymbol = row.get().get(symbolKey.get());
            currency.set(new Currency(currencyName, new Currency.Symbol(currencySymbol), currencyDescription.getId()));
        });
        if (currency.get() == null){
            throw new CurrencyNotFoundException(String.format("Did not find currency: %s", currencyDescription.getId()));
        }
        return currency.get();
    }

    private boolean isDatabaseInitiated() throws InitializationStatusCheckException, IOException {
        URL requestURL = new URL(databaseEndpoint, DatabaseInitiated.getCommand());
        WebRequest webRequest = new WebRequest(requestURL, HttpMethod.GET);
        WebClient webClient = new WebClient();
        webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
        Page page = webClient.getPage(webRequest);
        WebResponse response = page.getWebResponse();
        if (response.getStatusCode() >= 300){
            throw new InitializationStatusCheckException(response.getStatusMessage());
        }
        return Boolean.parseBoolean(response.getContentAsString());
    }

    private static Optional<String> findKey(Map<String, String> map, String keyName){
        Optional<String> key = map.keySet().stream().filter(currentKey -> currentKey.equalsIgnoreCase(keyName)).findAny();
        return key;
    }

    private Set<CollectorPlugin> loadCollectorPlugins() throws IOException, DatabaseNotInitiatedException {
        Set<CollectorPlugin> plugins = new HashSet<>();

        queryTable(new CollectorPluginTable().getName()).forEach(responseEntry -> {
            assert responseEntry.getTable().equalsIgnoreCase(new CollectorPluginTable().getName());
            responseEntry.getEntries().forEach(entry -> entry.entrySet().stream()
                    .filter(columnValue -> columnValue.getKey().equalsIgnoreCase(CollectorPluginTable.CODE.getName()))
                    .forEach(rowEntry -> {
                        try {
                            CollectorPlugin collectorPlugin = new PluginLoader().load(rowEntry.getValue(), CollectorPlugin.class);
                            plugins.add(collectorPlugin);
                        } catch (PluginLoader.LoadingException exception) {
                            LOGGER.error(String.format("Could not load plugin:\n%s", exception.getMessage()));
                        }
                    }));
        });
        return plugins;
    }
}
