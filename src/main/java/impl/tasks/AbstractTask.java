package impl.tasks;

import java.util.concurrent.atomic.AtomicBoolean;

public abstract class AbstractTask implements Task{
    protected final AtomicBoolean finished = new AtomicBoolean(false);
    protected final AtomicBoolean aborted = new AtomicBoolean(false);

    @Override
    public boolean isFinished() {
        return finished.get();
    }

    @Override
    public boolean isAborted() {
        return aborted.get();
    }
}
