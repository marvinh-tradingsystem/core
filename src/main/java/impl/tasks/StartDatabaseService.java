package impl.tasks;

import impl.Backend;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public class StartDatabaseService extends AbstractTask{
    private static final Logger LOGGER = LoggerFactory.getLogger(StartDatabaseService.class);

    private final File databaseFile;

    public StartDatabaseService(File databaseFile) {
        assert databaseFile != null;
        this.databaseFile = databaseFile;
    }

    @Override
    public void run() {
        Backend.activateDatabaseService(databaseFile);
        LOGGER.info("Database role activated.");
        this.finished.set(true);
    }
}
