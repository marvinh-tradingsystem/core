package impl.tasks;

import impl.userinterface.Webfrontend;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import socket.Port;

public class StartFrontend extends AbstractTask{
    private static final Logger LOGGER = LoggerFactory.getLogger(StartFrontend.class);

    private final Port port;

    public StartFrontend(Port port) {
        assert port != null;
        this.port = port;
    }

    @Override
    public void run() {
        try {
            Webfrontend.run(port);
            LOGGER.info(String.format("Frontend started on port %d", port.getNumber()));
            finished.set(true);
        } catch (Exception exception) {
            LOGGER.error(exception.getMessage());
            aborted.set(true);
        }
    }
}
