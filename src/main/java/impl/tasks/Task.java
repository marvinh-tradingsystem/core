package impl.tasks;

public interface Task extends Runnable{
    boolean isFinished();
    boolean isAborted();
}
