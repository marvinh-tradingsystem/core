package impl.config.loader;

import impl.config.InvalidConfigurationException;
import impl.config.settings.BackendConfiguration;
import impl.config.settings.Configuration;
import impl.config.settings.FrontendConfiguration;
import impl.config.settings.roles.CollectorRoleConfiguration;
import impl.config.settings.roles.DatabaseRoleConfiguration;
import impl.config.settings.roles.collector.CollectorConfiguration;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import socket.Port;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class XmlConfigurationLoader implements ConfigurationLoader {
    private final Configuration configuration;

    public XmlConfigurationLoader(Document document) throws InvalidConfigurationException {
        Configuration configuration = new Configuration();

        configuration.setFrontend(parseFrontendConfiguration(document));
        configuration.setBackend(parseBackendConfiguration(document));

        assert document.getElementsByTagName("roles").getLength() == 1;
        assert document.getElementsByTagName("roles").item(0).getNodeType() == Node.ELEMENT_NODE;
        Element roles = (Element) document.getElementsByTagName("roles").item(0);

        configuration.setDatabaseRole(parseDatabaseRoleConfiguration(roles));

        assert roles.getElementsByTagName("collector").getLength() <= 1;
        CollectorRoleConfiguration collectorRoleConfiguration = null;
        if (roles.getElementsByTagName("collector").getLength() > 0){
            collectorRoleConfiguration = new CollectorRoleConfiguration();
            assert roles.getElementsByTagName("collector").item(0).getNodeType() == Node.ELEMENT_NODE;
            Element collectorNode = (Element) roles.getElementsByTagName("collector").item(0);

            assert collectorNode.hasAttribute("database_url");
            try {
                URL databaseEndpoint = new URL(collectorNode.getAttribute("database_url"));
                collectorRoleConfiguration.setDatabaseEndpoint(databaseEndpoint);
            } catch (MalformedURLException e) {
                throw new InvalidConfigurationException(e.getMessage());
            }

            Set<CollectorConfiguration> collectors = new HashSet<>();
            assert collectorNode.getElementsByTagName("collect").getLength() > 0;
            for (int i = 0; i < collectorNode.getElementsByTagName("collect").getLength(); i++){
                CollectorConfiguration collectorConfiguration = new CollectorConfiguration();

                assert collectorNode.getElementsByTagName("collect").item(i).getNodeType() == Node.ELEMENT_NODE;
                Element collect = (Element) collectorNode.getElementsByTagName("collect").item(i);
                boolean demoMode = false;
                if (collect.hasAttribute("demo_mode")){
                    demoMode = Boolean.parseBoolean(collect.getAttribute("demo_mode"));
                }
                collectorConfiguration.setDemoMode(demoMode);
                assert collect.hasAttribute("plugin");
                collectorConfiguration.setPluginName(collect.getAttribute("plugin"));

                assert collect.hasAttribute("url");
                try {
                    URL queryURL = new URL(collect.getAttribute("url"));
                    collectorConfiguration.setQueryURL(queryURL);
                } catch (MalformedURLException e) {
                    throw new InvalidConfigurationException(e.getMessage());
                }

                assert collectorNode.getElementsByTagName("listening").getLength() == 1;
                Element listening = (Element) collectorNode.getElementsByTagName("listening").item(0);
                assert listening.getElementsByTagName("blocked").getLength() < 2;
                CollectorConfiguration.ChangeListening changeListening = null;
                if (listening.getElementsByTagName("blocked").getLength() == 1){
                    changeListening = new CollectorConfiguration.ChangeListening();
                    changeListening.setType(CollectorConfiguration.ChangeListening.Type.BLOCKEDWAITING);

                    Element blocked = (Element) listening.getElementsByTagName("blocked").item(0);
                    assert blocked.getElementsByTagName("interval").getLength() == 1;
                    Element interval = (Element) blocked.getElementsByTagName("interval").item(0);

                    assert interval.hasAttribute("check");
                    String check = interval.getAttribute("check");
                    assert check.equals("DOM change");

                    CollectorConfiguration.Interval checkInterval = new CollectorConfiguration.Interval();
                    assert interval.getElementsByTagName("rate").getLength() == 1;
                    Element rate = (Element) interval.getElementsByTagName("rate").item(0);
                    checkInterval.setRate(Long.parseLong(rate.getTextContent()));
                    assert interval.getElementsByTagName("unit").getLength() == 1;
                    Element unit = (Element) interval.getElementsByTagName("unit").item(0);
                    Optional<TimeUnit> intervalUnit = Arrays.stream(TimeUnit.values()).filter(timeUnit -> timeUnit.name().equalsIgnoreCase(unit.getTextContent())).findAny();
                    if (!intervalUnit.isPresent()){
                        throw new InvalidConfigurationException(String.format("Unknown time unit: %s", unit.getTextContent()));
                    }
                    checkInterval.setUnit(intervalUnit.get());

                    changeListening.setDomChangeCheck(checkInterval);
                }
                assert changeListening != null;
                collectorConfiguration.setChangeListening(changeListening);

                Map<CollectorConfiguration.Product, CollectorConfiguration.Currency> productsAndCurrencies = new HashMap<>();
                assert collectorNode.getElementsByTagName("product").getLength() >= 1;
                for (int j = 0; j < collectorNode.getElementsByTagName("product").getLength(); j++){
                    assert collectorNode.getElementsByTagName("product").item(i).getNodeType() == Node.ELEMENT_NODE;
                    Element productNode = (Element) collectorNode.getElementsByTagName("product").item(i);

                    CollectorConfiguration.Product product = new CollectorConfiguration.Product();
                    assert productNode.hasAttribute("name");
                    String name = productNode.getAttribute("name");
                    product.setName(name);

                    CollectorConfiguration.Currency currency = new CollectorConfiguration.Currency();
                    assert productNode.getElementsByTagName("currency").getLength() == 1;
                    Element currencyNode = (Element) productNode.getElementsByTagName("currency").item(0);
                    assert currencyNode.hasAttribute("id");
                    String id = currencyNode.getAttribute("id");
                    currency.setId(id);

                    productsAndCurrencies.put(product, currency);
                }
                collectorConfiguration.setProductsAndCurrencies(productsAndCurrencies);

                collectors.add(collectorConfiguration);
            }
            collectorRoleConfiguration.setCollectors(collectors);
        }
        configuration.setCollectorRole(collectorRoleConfiguration);

        this.configuration = configuration;
    }

    @Nullable
    protected DatabaseRoleConfiguration parseDatabaseRoleConfiguration(Element roles) throws InvalidConfigurationException {
        DatabaseRoleConfiguration databaseRoleConfiguration = null;
        assert roles.getElementsByTagName("database").getLength() <= 1;
        if (roles.getElementsByTagName("database").getLength() > 0){
            databaseRoleConfiguration = new DatabaseRoleConfiguration();

            assert roles.getElementsByTagName("database").item(0).getNodeType() == Node.ELEMENT_NODE;
            Element database = (Element) roles.getElementsByTagName("database").item(0);
            if (!database.hasAttribute("file")){
                throw new InvalidConfigurationException("Missing argument 'file' for database role");
            }
            String fileName = database.getAttribute("file");
            databaseRoleConfiguration.setDatabaseFile(new File(fileName));
        }
        return databaseRoleConfiguration;
    }

    @NotNull
    protected BackendConfiguration parseBackendConfiguration(Document document) throws InvalidConfigurationException {
        BackendConfiguration backendConfiguration = new BackendConfiguration();
        assert document.getElementsByTagName("backend").getLength() == 1;
        assert document.getElementsByTagName("backend").item(0).getNodeType() == Node.ELEMENT_NODE;
        Element backendNode = (Element) document.getElementsByTagName("backend").item(0);
        if (!backendNode.hasAttribute("port")){
            throw new InvalidConfigurationException("No port number for the backend given");
        }
        Port backendPort = new Port(Integer.parseInt(backendNode.getAttribute("port")));
        backendConfiguration.setPort(backendPort);
        return backendConfiguration;
    }

    @NotNull
    protected FrontendConfiguration parseFrontendConfiguration(Document document) throws InvalidConfigurationException {
        FrontendConfiguration frontendConfiguration = new FrontendConfiguration();
        assert document.getElementsByTagName("frontend").getLength() == 1;
        assert document.getElementsByTagName("frontend").item(0).getNodeType() == Node.ELEMENT_NODE;
        Element frontendNode = (Element) document.getElementsByTagName("frontend").item(0);
        if (!frontendNode.hasAttribute("port")){
            throw new InvalidConfigurationException("No port number for the frontend given");
        }
        Port frontendPort = new Port(Integer.parseInt(frontendNode.getAttribute("port")));
        frontendConfiguration.setPort(frontendPort);
        return frontendConfiguration;
    }

    @NotNull
    public Configuration getConfiguration(){
        return configuration;
    }
}
