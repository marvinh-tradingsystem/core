package impl.config.loader;

import impl.config.settings.Configuration;
import org.jetbrains.annotations.NotNull;

public interface ConfigurationLoader {
    @NotNull Configuration getConfiguration();
}
