package impl.config.settings;

import impl.config.settings.roles.CollectorRoleConfiguration;
import impl.config.settings.roles.DatabaseRoleConfiguration;

import java.util.Optional;

public class Configuration {
    private FrontendConfiguration frontend;
    private BackendConfiguration backend;

    private DatabaseRoleConfiguration databaseRole;
    private CollectorRoleConfiguration collectorRole;

    public FrontendConfiguration getFrontend() {
        return frontend;
    }

    public void setFrontend(FrontendConfiguration frontend) {
        this.frontend = frontend;
    }

    public BackendConfiguration getBackend() {
        return backend;
    }

    public void setBackend(BackendConfiguration backend) {
        this.backend = backend;
    }

    public Optional<DatabaseRoleConfiguration> getDatabaseRole() {
        return Optional.ofNullable(databaseRole);
    }

    public void setDatabaseRole(DatabaseRoleConfiguration databaseRole){
        this.databaseRole = databaseRole;
    }

    public Optional<CollectorRoleConfiguration> getCollectorRole() {
        return Optional.ofNullable(collectorRole);
    }

    public void setCollectorRole(CollectorRoleConfiguration collectorRole){
        this.collectorRole = collectorRole;
    }
}
