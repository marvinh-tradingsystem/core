package impl.config.settings;

import socket.Port;

public class BackendConfiguration {
    private Port port;

    public Port getPort() {
        return port;
    }

    public void setPort(Port port) {
        this.port = port;
    }
}
