package impl.config.settings;

import socket.Port;

public class FrontendConfiguration {
    private Port port;

    public void setPort(Port port) {
        this.port = port;
    }

    public Port getPort() {
        return port;
    }
}
