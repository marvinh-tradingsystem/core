package impl.config.settings.roles;

import java.io.File;

public class DatabaseRoleConfiguration {
    private File databaseFile;

    public File getDatabaseFile() {
        return databaseFile;
    }

    public void setDatabaseFile(File databaseFile) {
        this.databaseFile = databaseFile;
    }
}
