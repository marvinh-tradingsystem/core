package impl.config.settings.roles.collector;

import java.net.URL;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class CollectorConfiguration {
    public static class Product {
        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class Currency {
        private String id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    public static class Interval {
        private long rate;
        private TimeUnit unit;

        public long getRate() {
            return rate;
        }

        public void setRate(long rate) {
            this.rate = rate;
        }

        public TimeUnit getUnit() {
            return unit;
        }

        public void setUnit(TimeUnit unit) {
            this.unit = unit;
        }
    }

    public static class ChangeListening{
        public enum Type {
            BLOCKEDWAITING
        }

        private Type type;
        private Interval domChangeCheck;

        public Type getType() {
            return type;
        }

        public void setType(Type type) {
            this.type = type;
        }

        public Interval getDomChangeCheck() {
            return domChangeCheck;
        }

        public void setDomChangeCheck(Interval domChangeCheck) {
            this.domChangeCheck = domChangeCheck;
        }
    }

    private Boolean demoMode;
    private URL queryURL;
    private ChangeListening changeListening;
    private Map<Product, Currency> productsAndCurrencies;
    private String pluginName;

    public Boolean getDemoMode() {
        return demoMode;
    }

    public void setDemoMode(Boolean demoMode) {
        this.demoMode = demoMode;
    }

    public URL getQueryURL() {
        return queryURL;
    }

    public void setQueryURL(URL queryURL) {
        this.queryURL = queryURL;
    }

    public ChangeListening getChangeListening() {
        return changeListening;
    }

    public void setChangeListening(ChangeListening changeListening) {
        this.changeListening = changeListening;
    }

    public Map<Product, Currency> getProductsAndCurrencies() {
        return productsAndCurrencies;
    }

    public void setProductsAndCurrencies(Map<Product, Currency> productsAndCurrencies) {
        this.productsAndCurrencies = productsAndCurrencies;
    }

    public String getPluginName() {
        return pluginName;
    }

    public void setPluginName(String pluginName) {
        this.pluginName = pluginName;
    }
}
