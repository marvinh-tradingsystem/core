package impl.config.settings.roles;

import impl.config.settings.roles.collector.CollectorConfiguration;

import java.net.URL;
import java.util.Set;

public class CollectorRoleConfiguration {
    private URL databaseEndpoint;
    private Set<CollectorConfiguration> collectors;

    public URL getDatabaseEndpoint() {
        return databaseEndpoint;
    }

    public void setDatabaseEndpoint(URL databaseEndpoint) {
        this.databaseEndpoint = databaseEndpoint;
    }

    public Set<CollectorConfiguration> getCollectors() {
        return collectors;
    }

    public void setCollectors(Set<CollectorConfiguration> collectors) {
        this.collectors = collectors;
    }
}
