package impl.userinterface;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.webapp.WebAppContext;
import socket.Port;

import static org.eclipse.jetty.webapp.Configuration.ClassList;

public class Webfrontend {
    private static Server server;

    private static Port port;

    public static void run(Port port) throws Exception {
        assert port != null;
        Webfrontend.port = port;

        server = createServer(port);
        server.start();
    }

    public static Port getPort(){
        return Webfrontend.port;
    }

    private static Server createServer(Port port){
        Server server = new Server();
        ServerConnector connector = new ServerConnector(server);
        connector.setPort(port.getNumber());
        server.setConnectors(new Connector[] {connector});
        HandlerCollection frontendHandlers = new HandlerCollection();
        frontendHandlers.addHandler(createJSPHandler(server));
        frontendHandlers.addHandler(createResourceHandler());
        server.setHandler(frontendHandlers);
        return server;
    }

    protected static ContextHandler createResourceHandler(){
        ResourceHandler resourceHandler= new ResourceHandler();
        resourceHandler.setResourceBase("src/main/resources/web");
        resourceHandler.setDirectoriesListed(true);
        /*MimeTypes mimeTypes = new MimeTypes();
        mimeTypes.addMimeMapping(".css", "text/css");
        mimeTypes.addMimeMapping(".js", "text/javascript");
        resourceHandler.setMimeTypes(mimeTypes);*/
        ContextHandler contextHandler = new ContextHandler("/resources/web");
        contextHandler.setHandler(resourceHandler);
        return contextHandler;
    }

    protected static WebAppContext createJSPHandler(Server server) {
        WebAppContext ctx = new WebAppContext();
        ctx.setResourceBase("src/main/webapp");
        ctx.setContextPath("/trading");
        ctx.setAttribute("org.eclipse.jetty.server.webapp.ContainerIncludeJarPattern",".*/[^/]*jstl.*\\.jar$");
        ClassList classlist = ClassList.setServerDefault(server);
        classlist.addAfter(
                "org.eclipse.jetty.webapp.FragmentConfiguration",
                "org.eclipse.jetty.plus.webapp.EnvConfiguration",
                "org.eclipse.jetty.plus.webapp.PlusConfiguration"
        );
        classlist.addBefore(
                "org.eclipse.jetty.webapp.JettyWebXmlConfiguration",
                "org.eclipse.jetty.annotations.AnnotationConfiguration"
        );
        return ctx;
    }
}
