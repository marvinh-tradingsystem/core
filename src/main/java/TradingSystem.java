import impl.config.InvalidConfigurationException;
import impl.config.loader.XmlConfigurationLoader;
import impl.config.settings.Configuration;
import impl.tasks.StartBackend;
import impl.tasks.StartCollectorService;
import impl.tasks.StartDatabaseService;
import impl.tasks.StartFrontend;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.ScheduledThreadPoolExecutor;

/**
 * This is the main class of the trading system.
 */
public class TradingSystem {
    private static final Logger LOGGER = LoggerFactory.getLogger(TradingSystem.class);

    //flags
    public static final Object SHUTDOWN = new Object();

    //status codes
    private static final int INVALID_CONFIGURATION_FILE = 1;

    private static final ScheduledThreadPoolExecutor threadPoolExecutor = new ScheduledThreadPoolExecutor(1);

    /**
     * The main method which is called at application startup
     * @param args command-line arguments
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        File configurationFile = new File("config.xml");
        if (!configurationFile.exists()){
            FileUtils.copyToFile(Objects.requireNonNull(ClassLoader.getSystemResourceAsStream("default-config.xml")), configurationFile);
        }

        Configuration configuration = null;
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document configurationDocument = builder.parse(configurationFile);
            configuration = new XmlConfigurationLoader(configurationDocument).getConfiguration();
        } catch (ParserConfigurationException | SAXException e) {
            LOGGER.error(String.format("Error while parsing configuration file (%s):\n%s", configurationFile, e.getMessage()));
            System.exit(INVALID_CONFIGURATION_FILE);
        } catch (InvalidConfigurationException e) {
            LOGGER.error(String.format("Invalid configuration file: %s", e.getMessage()));
            System.exit(INVALID_CONFIGURATION_FILE);
        }

        StartFrontend startFrontend = new StartFrontend(configuration.getFrontend().getPort());
        threadPoolExecutor.submit(startFrontend);
        StartBackend startBackend = new StartBackend(configuration.getBackend().getPort());
        threadPoolExecutor.submit(startBackend);

        StartDatabaseService startDatabaseService = null;
        if (configuration.getDatabaseRole().isPresent()) {
            startDatabaseService = new StartDatabaseService(configuration.getDatabaseRole().get().getDatabaseFile());
            threadPoolExecutor.submit(startDatabaseService);
        }

        StartCollectorService startCollectorService = null;
        if (configuration.getCollectorRole().isPresent()) {
            startCollectorService = new StartCollectorService(startDatabaseService, startBackend, configuration.getCollectorRole().get(), threadPoolExecutor);
            threadPoolExecutor.submit(startCollectorService);
        }

        synchronized (SHUTDOWN){
            SHUTDOWN.wait();
        }
    }
}
