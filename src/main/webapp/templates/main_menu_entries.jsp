<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<jsp:useBean id="mainMenuOrigin" class="beans.Origin" scope="session" />
<div class="main_menu_entry <%
    if (mainMenuOrigin.getOriginPage().equals("/trading/manage/status.jsp")){
        out.print(" selected");
    }
%>"><a href="../manage/status.jsp">Status</a></div>
<div class="main_menu_entry<%
    if (mainMenuOrigin.getOriginPage().equals("/trading/manage/collector.jsp")){
        out.print(" selected");
    }
%>"><a href="../manage/collector.jsp">Collector</a></div>
<div class="main_menu_entry<%
    if (mainMenuOrigin.getOriginPage().equals("/trading/manage/database.jsp")){
        out.print(" selected");
    }
%>"><a href="../manage/database.jsp">Database</a></div>
<div class="main_menu_entry<%
%>"><a href="">Bank</a></div>