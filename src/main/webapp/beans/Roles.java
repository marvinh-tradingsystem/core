package beans;

public class Roles {
    private boolean database = false;

    public boolean isDatabase() {
        return database;
    }

    public void setDatabase(boolean database) {
        this.database = database;
    }
}
