package beans;

public class Origin {
    private String originPage = "null";

    public String getOriginPage() {
        return originPage;
    }

    public void setOriginPage(String originPage) {
        this.originPage = originPage;
    }
}
