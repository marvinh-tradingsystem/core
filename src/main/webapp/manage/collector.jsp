<%@ page import="impl.Backend" %>
<%@ page import="service.collector.command.AddCollectorPlugin" %>
<%@ page import="service.collector.command.DeleteCollectorPlugin" %>
<%@ page import="service.collector.command.QueryCollectorPlugins" %>
<%@ page import="service.collector.command.UpdateCollectorPlugin" %>
<%@ page import="java.net.URL" %>

<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<jsp:useBean id="mainMenuOrigin" class="beans.Origin" scope="session" />
<jsp:setProperty name="mainMenuOrigin" property="originPage" value="/trading/manage/collector.jsp" />
<!DOCTYPE html>
<html lang="en">
<jsp:include page="../templates/head.jsp" />
<body>
<script>
    let collector_page_details = [];

    window.onload = function () {
        show_collector_plugins();
    }

    function get_api_url(){
        return '<%
        URL requestURL = new URL(request.getRequestURL().toString());
        URL apiURL = new URL(requestURL.getProtocol(), requestURL.getHost(), Backend.getPort().getNumber(), "");
        out.print(apiURL.toString());
        %>';
    }

    function get_collector_plugin_addition_url(){
        return '<%
        URL addCollectorPluginURL = new URL(apiURL, AddCollectorPlugin.getCommand());
        out.print(addCollectorPluginURL);
        %>';
    }

    function get_show_collector_plugins_url(){
        return '<%
        URL showCollectorPluginsURL = new URL(apiURL, QueryCollectorPlugins.getCommand());
        out.print(showCollectorPluginsURL.toString());
        %>';
    }

    function get_delete_collector_plugin_url(){
        return '<%
        URL deleteCollectorPluginURL = new URL(apiURL, DeleteCollectorPlugin.getCommand());
        out.print(deleteCollectorPluginURL.toString());
        %>';
    }

    function get_update_collector_plugin_url(){
        return '<%
        URL updateCollectorPluginURL = new URL(apiURL, UpdateCollectorPlugin.getCommand());
        out.print(updateCollectorPluginURL.toString());
        %>';
    }

    function loadFile() {
        let sourceElement = document.getElementById('plugin_to_load');
        let file = sourceElement.files.item(0);

        // Check for the various File API support.
        if (!(window.File && window.FileReader && window.FileList && window.Blob)) {
            alert('The File APIs are not fully supported in this browser.');
        }

        document.getElementById('add_plugin_button').disabled = true;

        // Now read the file
        const fileReader = new FileReader();
        fileReader.onloadend = function(){
            document.getElementById('plugin_to_add').value = fileReader.result;
            document.getElementById('add_plugin_button').disabled = false;
        };
        fileReader.readAsText(file);
    }

    function add_collector_plugin() {
        let code = document.getElementById('plugin_to_add').value;

        let request_parameters = {};
        request_parameters.code = code;

        let url = get_collector_plugin_addition_url();
        let request;
        if (window.XMLHttpRequest) {
            request = new XMLHttpRequest();
        }
        if (request == null){
            window.alert('Request could not be sent because the browser does not support XMLHttpRequest.');
        }
        request.addEventListener('loadend', function () {
            if (request.status >= 200 && request.status < 300){
                show_collector_page_selection();
                show_collector_plugins();
            }
        });
        request.open('PUT', url, true);
        request.send(JSON.stringify(request_parameters));
    }

    function show_collector_plugins() {
        const targetURL = get_show_collector_plugins_url();
        let request;
        if (window.XMLHttpRequest) {
            request = new XMLHttpRequest();
        }
        if (request == null){
            window.alert('Request could not be sent because the browser does not support XMLHttpRequest.');
        }
        request.addEventListener('loadend', function () {
            if (request.status >= 200 && request.status < 300){
                remove_children('collector_plugin_table')

                let table = document.getElementById('collector_plugin_table');

                let response = JSON.parse(request.responseText);
                let table_head = document.createElement("thead");
                let head_row = document.createElement("tr");
                for (let index = 0; index < response.columns.length; index++) {
                    let column_name = response.columns[index]
                    let head_cell = document.createElement("th");
                    head_cell.textContent = column_name;
                    head_row.appendChild(head_cell);
                }
                let action_cell_head = document.createElement("th");
                head_row.appendChild(action_cell_head);
                table_head.appendChild(head_row);
                table.appendChild(table_head);

                let table_body = document.createElement("tbody");
                for (let row_index = 0; row_index < response.pluginInfos.length; row_index++){
                    let row = response.pluginInfos[row_index];
                    let table_row = document.createElement("tr");
                    for (let index = 0; index < response.columns.length; index++) {
                        let column_name = response.columns[index];
                        let value = row[column_name];
                        let table_cell = document.createElement("td");
                        let input_field = document.createElement('input');
                        input_field.setAttribute('type', 'text');
                        input_field.setAttribute('value', value);
                        input_field.setAttribute('class', 'input_field');
                        table_cell.appendChild(input_field);
                        table_row.appendChild(table_cell)
                    }
                    let action_cell = document.createElement("td");
                    action_cell.setAttribute('class', "action_cell");

                    let show_code_button = document.createElement("button");
                    show_code_button.textContent = "Show code";
                    show_code_button.onclick = function(){
                        show_code(row['name']);
                    }
                    action_cell.appendChild(show_code_button);

                    let delete_plugin_button = document.createElement("button");
                    delete_plugin_button.textContent = "Delete";
                    delete_plugin_button.onclick = function(){
                        delete_collector_plugin(row['name']);
                    }
                    action_cell.appendChild(delete_plugin_button);

                    table_row.appendChild(action_cell);
                    table_body.appendChild(table_row);
                }
                table.appendChild(table_body);
            } else if (request.status !== 0){
                window.alert('Error: '+request.status+' - '+request.statusText);
            }
        });
        request.open('GET', targetURL, true);
        request.send();
    }

    function delete_collector_plugin(plugin_name) {
        let request_parameters = {};
        request_parameters.name = plugin_name;

        const url = get_delete_collector_plugin_url();
        let request;
        if (window.XMLHttpRequest) {
            request = new XMLHttpRequest();
        }
        if (request == null){
            window.alert('Request could not be sent because the browser does not support XMLHttpRequest.');
        }
        request.addEventListener('loadend', function () {
            if (request.status >= 200 && request.status < 300) {
                show_collector_page_selection();
                show_collector_plugins();
            } else if (request.status !== 0){
                window.alert('Error: '+request.status+' - '+request.statusText);
            }
        });
        request.open('POST', url, true);
        request.send(JSON.stringify(request_parameters));
    }

    function show_code(plugin_name){
        let dialog = document.getElementById("edit_collector_plugin_dialog");

        let legend = dialog.getElementsByTagName("legend")[0];
        legend.textContent = '';

        let textarea = document.getElementById("collector_plugin_source_viewer");
        textarea.textContent = '';

        let request_parameters = [];
        request_parameters.push('code_for='+encodeURIComponent(plugin_name));
        const request_parameter_string = request_parameters.join("&");

        const targetURL = get_show_collector_plugins_url()+'?'+request_parameter_string;
        let request;
        if (window.XMLHttpRequest) {
            request = new XMLHttpRequest();
        }
        if (request == null){
            window.alert('Request could not be sent because the browser does not support XMLHttpRequest.');
        }
        request.addEventListener('loadend', function () {
            if (request.status >= 200 && request.status < 300) {
                let response = JSON.parse(request.responseText);
                let code = response.pluginSourceCodes[plugin_name];

                let dialog = document.getElementById("edit_collector_plugin_dialog");

                let legend = dialog.getElementsByTagName("legend")[0];
                legend.textContent = 'Edit collector plugin "'+plugin_name+'"';

                let textarea = document.getElementById("collector_plugin_source_viewer");
                textarea.textContent = code;

                let update_button = document.getElementById("update_collector_plugin_button");
                update_button.onclick = function () {
                    update_plugin(plugin_name);
                }

                dialog.showModal();
            }
        });
        request.open('GET', targetURL, true);
        request.send();
    }

    function update_plugin(plugin_name) {
        let textarea = document.getElementById("collector_plugin_source_viewer");
        let code = textarea.value;

        let request_parameters = {};
        request_parameters.oldName = plugin_name;
        request_parameters.newCode = code;

        const targetURL = get_update_collector_plugin_url();
        let request;
        if (window.XMLHttpRequest) {
            request = new XMLHttpRequest();
        }
        if (request == null){
            window.alert('Request could not be sent because the browser does not support XMLHttpRequest.');
        }
        request.addEventListener('loadend', function () {
            if (request.status >= 200 && request.status < 300) {
                close_dialog();
                show_collector_page_selection();
                show_collector_plugins();
            } else if (request.status !== 0){
                window.alert('Error: '+request.status+' - '+request.statusText);
            }
        });
        request.open('POST', targetURL, true);
        request.send(JSON.stringify(request_parameters));
    }

    function remove_children(element_id){
        const node = document.getElementById(element_id);
        let child = node.lastElementChild
        while (child){
            node.removeChild(child);
            child = node.lastElementChild;
        }
    }

    function close_dialog() {
        const dialog = document.getElementById("edit_collector_plugin_dialog");
        dialog.close();
    }
</script>
<h1>Management Console</h1>
<h2>Collector menu</h2>
<div class="container">
<div class="main_menu">
    <jsp:include page="../templates/main_menu_entries.jsp" />
</div>
<div class="upload_form">
    <input type="hidden" id="plugin_to_add" name="plugin" value="">
    <div class="filename_chooser"><input type="file" id="plugin_to_load" value="." oninput="loadFile()"></div>
    <div><button id="add_plugin_button" onclick="add_collector_plugin()">Upload plugin</button></div>
</div>
</div>
<h3>Plugins</h3>
<div>
    <dialog id="edit_collector_plugin_dialog">
        <fieldset>
            <legend></legend>
            <div class="collector_plugin_source_container"><textarea id="collector_plugin_source_viewer"></textarea></div>
            <div class="action_cell">
                <button id="update_collector_plugin_button">Update plugin</button>
                <button id="close_dialog_button" onclick="close_dialog()">Close dialog</button>
            </div>
        </fieldset>
    </dialog>
    <table id="collector_plugin_table">
    </table>
</div>
</body>
</html>