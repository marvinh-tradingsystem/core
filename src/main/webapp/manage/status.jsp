<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<jsp:useBean id="mainMenuOrigin" class="beans.Origin" scope="session" />
<jsp:setProperty name="mainMenuOrigin" property="originPage" value="/trading/manage/status.jsp" />
<!DOCTYPE html>
<html lang="en">
<jsp:include page="../templates/head.jsp" />
<body>
<h1>Management Console</h1>
<h2>Status page</h2>
<div class="container">
<div class="main_menu">
<jsp:include page="../templates/main_menu_entries.jsp" />
</div>
</div>
<div>
<!--TODO: add system information-->
</div>
</body>
</html>