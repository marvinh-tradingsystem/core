<%@ page import="impl.Backend" %>
<%@ page import="java.net.URL" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<jsp:useBean id="mainMenuOrigin" class="beans.Origin" scope="session" />
<jsp:useBean id="roles" class="beans.Roles"/>
<jsp:setProperty name="mainMenuOrigin" property="originPage" value="/trading/manage/database.jsp" />
<!DOCTYPE html>
<html lang="en">
<jsp:include page="../templates/head.jsp" />
<body>
<script>
    table_selection_states = new Map();

    window.onload = function(){
        is_initiated();
        table_selection();
    }

    function get_api_url(){
        return '<%
        URL requestURL = new URL(request.getRequestURL().toString());
        out.print(String.format("%s://%s:%s/command", requestURL.getProtocol(), requestURL.getHost(), Backend.getPort().getNumber()));
        %>';
    }

    function init_database(){
        const targetURL = get_api_url()+'/database/init';
        let request;
        if (window.XMLHttpRequest) {
            request = new XMLHttpRequest();
        }
        if (request == null){
            window.alert('Request could not be sent because the browser does not support XMLHttpRequest.');
        }
        request.addEventListener('loadend', function () {
            if (request.status >= 200 && request.status < 300){
                location.reload();
            } else {
                window.alert('Error: '+request.status+' - '+request.statusText);
            }
        });
        request.open('POST', targetURL, true);
        request.send();
    }

    function table_selection() {
        const targetURL = get_api_url()+'/database/query?count_only=true';
        let request;
        if (window.XMLHttpRequest) {
            request = new XMLHttpRequest();
        }
        if (request == null){
            window.alert('Request could not be sent because the browser does not support XMLHttpRequest.');
        }
        request.addEventListener('loadend', function () {
            if (request.status >= 200 && request.status < 300){
                let table_list = document.getElementById('tables_to_query');

                let child = table_list.lastElementChild;
                while(child){
                    table_list.removeChild(child);
                    child = table_list.lastElementChild;
                }

                let tables = JSON.parse(request.responseText);
                for (let index = 0; index < tables.length; index++) {
                    let item = tables[index];
                    let table_name = item.table;
                    let id = 'table_'+index;
                    let checkbox = document.createElement('input');
                    checkbox.onclick = function () {
                        if (checkbox.checked){
                            table_selection_states.set(table_name, 'selected');
                        }
                        else {
                            table_selection_states.set(table_name, 'unselected');
                        }
                    };
                    checkbox.setAttribute('id', id);
                    checkbox.setAttribute('type', 'checkbox');
                    checkbox.setAttribute('name', table_name);
                    if (table_selection_states.has(table_name) && table_selection_states.get(table_name) === 'selected'){
                        checkbox.checked = true;
                    } else {
                        table_selection_states.set(table_name, 'unselected');
                    }
                    table_list.appendChild(checkbox);

                    let label = document.createElement('label');
                    label.setAttribute('for', id);
                    label.appendChild(document.createTextNode(item.table+' ('+item.rows+' rows)'));
                    table_list.appendChild(label);

                    table_list.appendChild(document.createElement('br'));
                }
            } else if (request.status !== 0){
                window.alert('Error: '+request.status+' - '+request.statusText);
            }
        });
        request.open('GET', targetURL, true);
        request.send();
    }

    function show_tables(){
        let table_regex_parts = [];

        for (const [key, value] of table_selection_states){
            if (value === 'selected'){
                table_regex_parts.push(key);
            }
        }
        if (table_regex_parts.length < 1){
            window.alert('No table selected');
            return;
        }

        const targetURL = get_api_url()+'/database/query?table='+encodeURIComponent(table_regex_parts.join('|'));
        let request;
        if (window.XMLHttpRequest) {
            request = new XMLHttpRequest();
        }
        if (request == null){
            window.alert('Request could not be sent because the browser does not support XMLHttpRequest.');
        }
        request.addEventListener('loadend', function () {
            if (request.status >= 200 && request.status < 300){
                let table_list = document.getElementById('query_results');

                let child = table_list.lastElementChild;
                while(child){
                    table_list.removeChild(child);
                    child = table_list.lastElementChild;
                }

                let rows = JSON.parse(request.responseText);
                for (let index = 0; index < rows.length; index++) {
                    let row = rows[index];
                    let table_container = document.createElement('div');
                    table_container.setAttribute('name', 'query');
                    table_container.setAttribute('class', 'database_entry_table');

                    let fieldset = document.createElement('fieldset');
                    table_container.appendChild(fieldset);

                    let legend = document.createElement('legend');
                    legend.textContent = 'New dataset for "'+row.table+'"';
                    fieldset.appendChild(legend);

                    let value_input_fields_container = document.createElement('div');
                    value_input_fields_container.setAttribute('class', 'value_input_fields');

                    let value_input_ids = new Map();
                    for (let column_index = 0; column_index < row.columns.length; column_index++){
                        let value_input_container = document.createElement('div');
                        value_input_container.setAttribute('class', 'column_value_input');

                        let label = document.createElement('label');
                        let input_id = 'col_'+index+'_'+column_index;
                        label.setAttribute('for', input_id);
                        label.textContent = row.columns[column_index].name;
                        value_input_container.appendChild(label);

                        let input_field = document.createElement('input');
                        input_field.setAttribute('id', input_id);
                        input_field.setAttribute('type', 'text');
                        value_input_container.appendChild(input_field);

                        value_input_ids.set(row.columns[column_index].name, input_id);

                        value_input_fields_container.appendChild(value_input_container);
                    }
                    fieldset.appendChild(value_input_fields_container);

                    let insert_row_submit_container = document.createElement('div');
                    insert_row_submit_container.setAttribute('class', 'insert_row_submit');
                    let insert_row_submit_button = document.createElement('button');
                    insert_row_submit_button.textContent = 'Insert row';
                    insert_row_submit_button.onclick = function () {
                        insert_row(row.table, value_input_ids);
                    }
                    insert_row_submit_container.appendChild(insert_row_submit_button);
                    fieldset.appendChild(insert_row_submit_container);

                    let table = document.createElement('table');
                    let caption = document.createElement('caption');
                    caption.textContent = row.table;
                    table.appendChild(caption);

                    let thead = document.createElement('thead');
                    let head_row = document.createElement('tr');
                    for (let column_index = 0; column_index < row.columns.length; column_index++){
                        let head_cell = document.createElement('th');
                        head_cell.textContent = row.columns[column_index].name;
                        head_row.appendChild(head_cell);
                    }
                    let action_cell_head = document.createElement('th');
                    head_row.appendChild(action_cell_head);
                    thead.appendChild(head_row);
                    table.appendChild(thead);

                    let tbody = document.createElement('tbody');
                    for (const entry of row.entries){
                        let table_row = document.createElement('tr');

                        const old_values = new Map();
                        const current_values = new Map();
                        for (const column of row.columns){
                            let column_name = column.name;
                            for (const key in entry){
                                if (key.toLowerCase() !== column_name.toLowerCase()){
                                    continue;
                                }
                                let value = entry[key];

                                old_values.set(column_name, value);
                                current_values.set(column_name, value);

                                let table_cell = document.createElement('td');

                                let input_field = document.createElement('input');
                                input_field.oninput = function(){
                                    current_values.set(column_name, input_field.value);
                                };
                                input_field.setAttribute('type', 'text');
                                input_field.setAttribute('value', value);
                                input_field.setAttribute('class', 'input_field');
                                table_cell.appendChild(input_field);

                                table_row.appendChild(table_cell);
                            }
                        }
                        let action_cell = document.createElement('td');
                        action_cell.setAttribute('class', 'action_cell');

                        let update_button = document.createElement('button');
                        update_button.onclick = function(){
                            update_row(row.table, old_values, current_values);
                        }
                        update_button.textContent = 'Update'
                        update_button.setAttribute('class', 'row_update_button');
                        action_cell.appendChild(update_button);

                        let delete_button = document.createElement('button');
                        delete_button.onclick = function(){
                            delete_row(row.table, old_values);
                        }
                        delete_button.textContent = 'Delete';
                        delete_button.setAttribute('class', 'row_deletion_button');
                        action_cell.appendChild(delete_button);

                        table_row.appendChild(action_cell);
                        tbody.appendChild(table_row);
                    }
                    table.appendChild(tbody);
                    table_container.appendChild(table);

                    table_list.appendChild(table_container);
                }

            } else if (request.status !== 0){
                window.alert('Error: '+request.status+' - '+request.statusText);
            }
        });
        request.open('GET', targetURL, true);
        request.send();
    }

    function update_row(table_name, old_values, new_values){
        let old_column_values = Object.create(null);
        for (const [column_name, value] of old_values.entries()) {
            old_column_values[column_name] = value;
        }

        let new_column_values = Object.create(null);
        for (const [column_name, value] of new_values.entries()) {
            new_column_values[column_name] = value;
        }

        let request_parameters = {};
        request_parameters.table = table_name;
        request_parameters.oldRow = old_column_values;
        request_parameters.newRow = new_column_values;

        const targetURL = get_api_url()+'/database/update';
        let request;
        if (window.XMLHttpRequest) {
            request = new XMLHttpRequest();
        }
        if (request == null){
            window.alert('Request could not be sent because the browser does not support XMLHttpRequest.');
        }
        request.addEventListener('loadend', function () {
            if (request.status >= 200 && request.status < 300){
                table_selection();
                show_tables();
                window.alert("Update successful.");
            }
        });
        request.open('POST', targetURL, true);
        request.send(JSON.stringify(request_parameters));
    }

    function delete_row(table_name, old_values){
        let column_values = Object.create(null);
        for (const [column_name, value] of old_values.entries()) {
            column_values[column_name] = value;
        }

        let request_parameters = {};
        request_parameters.table = table_name;
        request_parameters.values = column_values;

        const targetURL = get_api_url()+'/database/delete';
        let request;
        if (window.XMLHttpRequest) {
            request = new XMLHttpRequest();
        }
        if (request == null){
            window.alert('Request could not be sent because the browser does not support XMLHttpRequest.');
        }
        request.addEventListener('loadend', function () {
            if (request.status >= 200 && request.status < 300){
                table_selection();
                show_tables();
            }
        });
        request.open('POST', targetURL, true);
        request.send(JSON.stringify(request_parameters));
    }

    function insert_row(table_name, value_input_ids) {
        let column_values = Object.create(null);
        for (const [column_name, value_element_id] of value_input_ids.entries()) {
            let value_element = document.getElementById(value_element_id);
            let value = value_element.value;
            column_values[column_name] = value;
        }

        let request_parameters = {};
        request_parameters.table = table_name;
        request_parameters.values = column_values;

        const targetURL = get_api_url()+'/database/insert';
        let request;
        if (window.XMLHttpRequest) {
            request = new XMLHttpRequest();
        }
        if (request == null){
            window.alert('Request could not be sent because the browser does not support XMLHttpRequest.');
        }
        request.addEventListener('loadend', function () {
            if (request.status >= 200 && request.status < 300){
                table_selection();
                show_tables();
            }
        });
        request.open('PUT', targetURL, true);
        request.send(JSON.stringify(request_parameters));
    }

    function is_initiated() {
        const targetURL = get_api_url()+'/database/initiated';
        let request;
        if (window.XMLHttpRequest) {
            request = new XMLHttpRequest();
        }
        if (request == null){
            window.alert('Request could not be sent because the browser does not support XMLHttpRequest.');
        }
        request.addEventListener('loadend', function () {
            if (request.status >= 200 && request.status < 300){
                let checkbox = document.getElementById('initialized');
                let responseText = request.responseText.replace('\n', '');
                checkbox.checked = (responseText === 'true');
            } else if (request.status !== 0){
                window.alert('Error: '+request.status+' - '+request.statusText);
            }
        });
        request.open('GET', targetURL, true);
        request.send();
    }

    function export_database() {
        const targetURL = get_api_url()+'/database/export';
        const fileName = 'database.zip';

        let request;
        if (window.XMLHttpRequest) {
            request = new XMLHttpRequest();
        }
        if (request == null){
            window.alert('Request could not be sent because the browser does not support XMLHttpRequest.');
        }
        request.responseType = "blob";
        request.addEventListener('loadend', function () {
            if (request.status >= 200 && request.status < 300){
                const blob = this.response;

                const a = document.createElement('a');
                a.href = window.URL.createObjectURL(blob);
                a.download = fileName;
                a.click();
            } else if (request.status !== 0){
                window.alert('Error: '+request.status+' - '+request.statusText);
            }
        });
        request.open('GET', targetURL, true);
        request.send();
    }

    function import_database() {
        //TODO: implement
    }
</script>
<h1>Management Console</h1>
<h2>Database menu</h2>
<div class="container">
<div class="main_menu">
    <jsp:include page="../templates/main_menu_entries.jsp" />
</div>
<div class="upload_form">
    <div><input type="checkbox" id="initialized" autocomplete="off" disabled>
    <label for="initialized">Database initialized</label></div>
    <div><button onclick="init_database()">Init database</button></div>
    <div><button onclick="import_database()">Import database</button></div>
    <div><button onclick="export_database()">Export database</button></div>
</div>
</div>
<h3>Database entries</h3>
<div class="query_filter">
    <div class="upload_form">
        <div class="table_selection">
            <fieldset id="tables_to_query">
                <legend>Tables to query</legend>
            </fieldset>
        </div>
        <button onclick="show_tables()">Show</button>
    </div>
</div>
<div id="query_results" class="tables">
</div>
</body>
</html>